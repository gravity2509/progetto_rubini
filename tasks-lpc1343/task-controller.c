#include <bathos/bathos.h>
#include <bathos/delay.h>
#include <bathos/jiffies.h>
#include <bathos/gpio.h>
#include <arch/hw.h>

#include "control.h"

int servo_pos = 600;
int measure = 0;
enum bool sonar_on = false;
enum bool msg_ready = false;	

/*Get and Set for extern variable*/
enum bool get_sonar_on(){
	return sonar_on;
}

enum bool get_msg_ready(){
	return msg_ready;
}

int get_servo_pos(){
	return servo_pos;
}

void set_measure(int new){
	measure = new;
	msg_ready = true;
}

int get_measure(){
	msg_ready = false;
	return measure;
}

/* Init function*/
static void *control_init(void *arg)
{
	
	sonar_on = true;
}


static void *control(void *arg)
{
	if (msg_ready == true){
		sonar_on = false;
		if (servo_pos < 2400)
			servo_pos += 450;
		else
			servo_pos = 600;
	}
	else{
		sonar_on = true;	
	}		
}



static struct bathos_task __task t_servo_direction = {
	.name = "control", .period = HZ,
	.job = control, 
	.release = 4
};
