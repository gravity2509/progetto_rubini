/*
 * Servo motor
 * 2014 GNU GPL2 or later
 */
#include <bathos/bathos.h>
#include <bathos/delay.h>
#include <bathos/gpio.h>
#include <arch/hw.h>
#include <bathos/jiffies.h>

#include "control.h"

/* The length(us) of the pulse will determine how far the motor turns. 
 */  

static int servo_init(void *unused)
{
	gpio_init();
	/* Control pin */
	gpio_dir(GPIO_NR(0, 9), GPIO_DIR_OUT, 0);
	return 0;
}

static void *servo(void *arg)
{			
	unsigned long now;
	now = jiffies;
	gpio_set(GPIO_NR(0,9), 1);
	now += get_servo_pos()+2;
	while(time_before(jiffies, now))
		;
	gpio_set(GPIO_NR(0,9), 0);
}

/* The servo motor expects to see a pulse every 20 ms */
static struct bathos_task __task t_servo_pulse = {
	.name = "servo_pulse", .period = HZ/50,
	.init = servo_init, .job = servo, 
	.release = 2
};



