enum bool { false, true };

extern enum bool get_sonar_on(void);
extern enum bool get_msg_ready(void);

extern void set_measure(int);
extern int get_measure(void);

extern int get_servo_pos(void);
