#include <bathos/bathos.h>
#include <bathos/delay.h>
#include <bathos/jiffies.h>
#include <bathos/gpio.h>
#include <arch/hw.h>

#include "control.h"


static void *serial(void *arg)
{
	if (get_msg_ready() == true){
		printf("Sonar distance: %d\n",get_measure());
	}		
}



static struct bathos_task __task t_servo_direction = {
	.name = "serial", .period = HZ,
	.job = serial, 
	.release = 5
};
