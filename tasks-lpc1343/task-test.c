/*
 * A trivial task that flashes three leds
 * Alessandro Rubini, 2009 GNU GPL2 or later
 */
#include <bathos/bathos.h>
#include <bathos/gpio.h>
#include <arch/hw.h>

static void *test(void *arg)
{
	char *s = arg;
	puts(s);
	return arg;
}

static struct bathos_task __task t_test = {
	.name = "test", .period = HZ * 10,
	.job = test, .arg = "  test  ",
	.release = 10
};
