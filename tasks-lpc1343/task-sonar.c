/*
 * Sonar HC - SR04 
 * 2014 GNU GPL2 or later
 */
#include <bathos/bathos.h>
#include <bathos/delay.h>
#include <bathos/jiffies.h>
#include <bathos/gpio.h>
#include <arch/hw.h>

#include "control.h"

static int sonar_init(void *unused)
{
	gpio_init();
	/* Echo, to start the ranging supply a 10uS high pulse*/
	gpio_dir_af(GPIO_NR(0, 10), GPIO_DIR_IN, 0, GPIO_AF_GPIO);
	/* Trigger */
	gpio_dir_af(GPIO_NR(0, 11), GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	return 0;
}


static void *sonar(void *arg)
{
	if(get_sonar_on()==false)
		return 0;
	gpio_set(GPIO_NR(0,11), 1);
	udelay(10);
	gpio_set(GPIO_NR(0,11), 0);
	
	/* Dopo 60ms senza risposta sul echo si presume che la distanza sia 
	fuori range */
	unsigned long now;
	unsigned long timeout = 60*1000;
	int duration = 0;
	now = jiffies;
	timeout += now + 2;
	
	while(gpio_get(GPIO_NR(0,10)) == 0){
		if (time_after(jiffies, timeout)){
			printf("Out of range\n");
			return 1;
		}
	}
	
	now = jiffies;
	while(gpio_get(GPIO_NR(0,10)) == 1)
		;
	duration = jiffies - now;	
	int r = 0.034 * duration / 2;
	set_measure(r);
	return 0;	
}

/* Il datasheet suggerisce un periodo di misura non inferiore 60ms*/
static struct bathos_task __task t_sonar = {
	.name = "sonar", .period = HZ / 15,
	.init = sonar_init, .job = sonar, .arg=0,
	.release =3
};
